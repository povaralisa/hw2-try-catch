const listFilms = document.querySelector(".list");

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((item) =>
    item.forEach((film) => {
      const text = `<li class='movie' data-id='${film.id}'>${film.name}<p>Номер эпиода:${film.episodeId}</p><p>Описание :${film.openingCrawl}</p><p>Персонажи:</p></li>`;

      listFilms.insertAdjacentHTML("beforebegin", text);
      const heroes = [];
      const requests = [];

      film.characters.forEach((character) => {
        requests.push(
          fetch(character)
            .then((response) => response.json())
            .then((hero) => {
              heroes.push(hero.name);
            })
        );
      });
      Promise.all(requests).then(() => {
        let character = "";
        heroes.forEach((hero) => {
          character += `<span>${hero + ","}</span>`;
        });

        const filmItem = document.querySelector(`.movie[data-id='${film.id}']`);
        console.log(filmItem);
        filmItem.insertAdjacentHTML("beforeend", character);
      });
    })
  );
